package com.dakenico.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
public class HeadlessBrowserTest {

	public String pageTitleCS = "Calsense Command Center";
	private WebDriver driver;
	
	@Before
    public void setUp() throws MalformedURLException
    {
        driver = new HtmlUnitDriver(false);
        driver.get("http://web-ui.azurewebsites.net/login");
    }

    @Test
    public void landingPageTitleExists()
    {
    	String pageTitle = driver.getTitle();
    	assertEquals("Title not match or not found", pageTitleCS, pageTitle);
    	System.out.println("Title page text = " + pageTitle);
    }
	
    @After
    public void tearDown() throws Exception {
        driver.quit();
    }
}
